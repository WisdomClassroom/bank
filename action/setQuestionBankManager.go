/*******************************************************************************
 * Copyright 2020 huanggefan.cn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package action

import (
	"gitee.com/WisdomClassroom/bank/global"
	"gitee.com/WisdomClassroom/core"
	"gitee.com/WisdomClassroom/core/models"
	"gitee.com/WisdomClassroom/core/protobufs/v1/pb"
	"github.com/jinzhu/gorm"
)

func SetQuestionBankManager(req *pb.SetQuestionBankManagerRequest) *pb.SetQuestionBankManagerResponse {
	resp := new(pb.SetQuestionBankManagerResponse)
	var err error

	token := core.NewToken()
	err = token.UnpackToken(req.AuthToken, global.TokenCert)
	if err != nil {
		resp.Status = &pb.ResponseStatus{
			Code:    pb.ResponseStatusCode_OtherError,
			Message: "认证失败： " + err.Error(),
		}
		return resp
	}

	if token.Type != core.UserRoleTypeCodeRoot {
		resp.Status = &pb.ResponseStatus{
			Code:    pb.ResponseStatusCode_OtherError,
			Message: "没有权限",
		}
		return resp
	}

	bank := &models.BankModel{}
	err = global.DB.Where("uuid = ?", req.WillSetQuestionBankUUID).Not("deleted", true).First(bank).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			global.Logger.Debug("Find BankModel: record not found")
			resp.Status = &pb.ResponseStatus{
				Code:    pb.ResponseStatusCode_OtherError,
				Message: "题库不存在：" + req.WillSetQuestionBankUUID,
			}
		} else {
			global.Logger.Warning("Find BankModel error: " + err.Error())
			resp.Status = &pb.ResponseStatus{
				Code:    pb.ResponseStatusCode_OtherError,
				Message: "查找题库错误：" + err.Error(),
			}
		}
		return resp
	}

	user := &models.UserModel{}
	err = global.DB.Where("uuid = ?", req.WillSetManagerUUID).Not("deleted", true).First(user).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			resp.Status = &pb.ResponseStatus{
				Code:    pb.ResponseStatusCode_OtherError,
				Message: "未找到用户: " + req.WillSetManagerUUID,
			}
		} else {
			global.Logger.Warning("Find UserModel error: " + err.Error())
			resp.Status = &pb.ResponseStatus{
				Code:    pb.ResponseStatusCode_OtherError,
				Message: err.Error(),
			}
		}
		return resp
	}

	err = global.DB.
		Where(models.BankManagerBindModel{BankUUID: req.WillSetQuestionBankUUID, ManagerUUID: req.WillSetManagerUUID}).
		Not("deleted", true).
		First(&models.BankManagerBindModel{}).Error
	if err != gorm.ErrRecordNotFound {
		if err != nil {
			global.Logger.Warning("Find BankModel error: " + err.Error())
		}
		resp.Status = &pb.ResponseStatus{
			Code:    pb.ResponseStatusCode_OtherError,
			Message: "用户已经为该题库管理员",
		}
		return resp
	}

	bankManager := &models.BankManagerBindModel{
		Model:       models.NewModel(),
		BankUUID:    req.WillSetQuestionBankUUID,
		ManagerUUID: req.WillSetManagerUUID,
	}
	err = global.DB.Create(bankManager).Error
	if err != nil {
		resp.Status = &pb.ResponseStatus{
			Code:    pb.ResponseStatusCode_OtherError,
			Message: "创建题库管理员失败：" + err.Error(),
		}
		return resp
	}

	resp.Status = &pb.ResponseStatus{
		Code:    pb.ResponseStatusCode_Suc,
		Message: "创建题库管理员成功",
	}
	return resp
}
