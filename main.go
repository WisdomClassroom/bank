/*******************************************************************************
 * Copyright 2020 huanggefan.cn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package main

import (
	"context"
	"flag"
	"gitee.com/WisdomClassroom/core/models"
	"github.com/jinzhu/gorm"
	"log"
	"net"
	"net/http"
	"runtime"
	"sync"
	"time"

	grpcRuntime "github.com/grpc-ecosystem/grpc-gateway/runtime"
	"google.golang.org/grpc"

	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/jinzhu/gorm/dialects/sqlite"

	"gitee.com/WisdomClassroom/bank/global"
	"gitee.com/WisdomClassroom/bank/service"
	"gitee.com/WisdomClassroom/core"
	"gitee.com/WisdomClassroom/core/protobufs/v1/pb"
)

var (
	BuildTag  string = ""
	BuildGo   string = ""
	BuildTime string = ""
)

func runGateway(wg *sync.WaitGroup) {
	defer wg.Done()

	headerMatcher := func(key string) (string, bool) {
		return key, true
	}

	gateway := grpcRuntime.NewServeMux(grpcRuntime.WithIncomingHeaderMatcher(headerMatcher),
		grpcRuntime.WithMarshalerOption(grpcRuntime.MIMEWildcard,
			&grpcRuntime.JSONPb{OrigName: true, EmitDefaults: true}))
	opts := []grpc.DialOption{grpc.WithInsecure()}
	err := pb.RegisterQuestionBankServiceHandlerFromEndpoint(context.Background(), gateway, core.FlagGRPCListen, opts)
	if err != nil {
		global.Logger.Error(err.Error())
		return
	}

	mux := http.NewServeMux()
	mux.Handle("/api/", gateway)

	err = http.ListenAndServe(core.FlagHttpListen, mux)
	if err != nil {
		global.Logger.Error(err.Error())
		return
	}
}

func runServer(wg *sync.WaitGroup) {
	defer wg.Done()

	listen, err := net.Listen("tcp", core.FlagGRPCListen)
	if err != nil {
		global.Logger.Error(err.Error())
		return
	}

	grpcServer := grpc.NewServer()
	pb.RegisterQuestionBankServiceServer(grpcServer, &service.Service{})

	if err = grpcServer.Serve(listen); err != nil {
		global.Logger.Error(err.Error())
		return
	}
}

func main() {
	core.PublicFlag(BuildTag, BuildGo, BuildTime)
	core.PublicServerFlag()
	flag.Parse()

	if core.FlagHelp {
		flag.Usage()
		return
	}
	if core.FlagVersion {
		core.PublicVersion()
		return
	}
	if !core.FlagUseSqlite && !core.FlagUsePG {
		flag.Usage()
		return
	}

	if core.FlagLogWithUDP {
		addr, err := net.ResolveUDPAddr("udp", core.FlagLogAddr)
		if err != nil {
			log.Fatalln(err)
		}
		global.Logger, err = core.NewUDPLogger(core.FlagLogLevel, addr, 10240)
		if err != nil {
			log.Fatalln(err)
		}
	} else {
		global.Logger = core.NewLogger(core.FlagLogLevel)
	}

	if core.FlagPPROFEnable {
		go core.StartPPOFDebug(http.Server{Addr: core.FlagPPROFListen}, true)
		time.Sleep(1 * time.Second)
	}

	global.Logger.Info("http listen at: http://" + core.FlagHttpListen)
	global.Logger.Info("grpc listen at: grpc://" + core.FlagGRPCListen)
	if core.FlagLogWithUDP {
		global.Logger.Info("log to: udp://" + core.FlagLogAddr)
	}

	var err error
	if core.FlagUseSqlite {
		global.DB, err = gorm.Open("sqlite3", core.FlagSqliteDB)
		if err != nil {
			errInfo := "failed to open database: " + err.Error()
			global.Logger.Error(errInfo)
			return
		}
	} else if core.FlagUsePG {
		global.DB, err = gorm.Open("postgres", models.GetPgSQLConf())
		if err != nil {
			errInfo := "failed to connect database: " + err.Error()
			global.Logger.Error(errInfo)
			return
		}
	} else {
		flag.Usage()
		return
	}
	global.TokenCert = core.NewTokenCert(core.FlagTokenCert)

	if core.FlagLogLevel == core.LogDebug {
		global.DB.LogMode(true)
		global.DB.SetLogger(global.Logger)
	}

	wg := new(sync.WaitGroup)
	wg.Add(2)
	go runGateway(wg)
	go runServer(wg)
	wg.Wait()
}

func init() {
	runtime.GOMAXPROCS(1)
}
