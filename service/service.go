/*******************************************************************************
 * Copyright 2020 huanggefan.cn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package service

import (
	"context"
	"gitee.com/WisdomClassroom/bank/action"
	"gitee.com/WisdomClassroom/core/protobufs/v1/pb"
)

type Service struct{}

func (s Service) GetQuestionBank(ctx context.Context, req *pb.GetQuestionBankRequest) (*pb.GetQuestionBankResponse, error) {
	resp := action.GetQuestionBank(req)
	return resp, nil
}

func (s Service) ListQuestionBank(ctx context.Context, req *pb.ListQuestionBankRequest) (*pb.ListQuestionBankResponse, error) {
	resp := action.ListQuestionBank(req)
	return resp, nil
}

func (s Service) CreateQuestionBank(ctx context.Context, req *pb.CreateQuestionBankRequest) (*pb.CreateQuestionBankResponse, error) {
	resp := action.CreateQuestionBank(req)
	return resp, nil
}

func (s Service) UpdateQuestionBank(ctx context.Context, req *pb.UpdateQuestionBankRequest) (*pb.UpdateQuestionBankResponse, error) {
	resp := action.UpdateQuestionBank(req)
	return resp, nil
}

func (s Service) DeleteQuestionBank(ctx context.Context, req *pb.DeleteQuestionBankRequest) (*pb.DeleteQuestionBankResponse, error) {
	resp := action.DeleteQuestionBank(req)
	return resp, nil
}

func (s Service) SetQuestionBankManager(ctx context.Context, req *pb.SetQuestionBankManagerRequest) (*pb.SetQuestionBankManagerResponse, error) {
	resp := action.SetQuestionBankManager(req)
	return resp, nil
}

func (s Service) UnSetQuestionBankManager(ctx context.Context, req *pb.UnSetQuestionBankManagerRequest) (*pb.UnSetQuestionBankManagerResponse, error) {
	resp := action.UnSetQuestionBankManager(req)
	return resp, nil
}
